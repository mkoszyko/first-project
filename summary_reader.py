import re

def parse_summary_file(summary_file):
    """
    Parses the summary file and returns the contents in the form fo dictionary.
    :param summary_file: contents of the summary file.
    :return: dictionary: the contents of the summary file in the form of dictionary.
    """

    def find_params(summary_file, param):
        """
        Finds parameters of in the summary file contents, like LocusID, VenueID, StartTime, StopTime.
        :param summary_file: the contents of the summary file.
        :return: param: found parameter, like LocusID, VenueID, StartTime, StopTime.
        """
        regex = '^\*\*' + param + ':\*\*\s+(.+)$'
        for line in summary_file:
            param_list = re.match(regex, line)
            if param_list:
                param = param_list.group(1)
        return param

    def find_call_type(summary_file):
        """
        Finds CallType in the summary file contents.
        :param summary_file: the contents of the summary file.
        :return: call_type: value of the CallType.
        """
        call_type = ''
        regex = '^Call Type:\s+(\w+)$'
        for line in summary_file:
            call_type_list = re.match(regex, line)
            if call_type_list:
                call_type = call_type_list.group(1)
        return call_type

    def parse_sections(summary_file, section_name):

        """
        Parse the summary looking for a specific section_name
        :param summary_file_path: path of the directory that contains the summary file.
        :return: list containing lines of the specified section of the summary file
        """
        section = []

        if section_name == "**TRANSCODERS**":
            if "**NO TRANSCODERS**" in summary_file:
                return "None"
        delimiter = "-----"
        found_section = "false"
        for line in summary_file:
            if delimiter in line:
                found_section = "false"
            if found_section == "true":
                section.append(line)
            if section_name in line:
                found_section = "true"
        return section

    def parse_confluences_section(section):
        """
        Parses the confluence section and outputs: ClientType, IsGateway, TrackinID, CorrelationID, PairedWith
        values in the form of dictionary.
        :param section: contents of the confluence section.
        :return: confluence_dict: a dictionary with the values extracted form the cascades section of the
        summary file.
        """
        confluence_dict = {}
        regex_media_node = '\* \*{2}(\S+).*\*\*'
        regex_list = '^\|(0-\d+) \| (\S+) \| (\S+) \| (\S+) \| (\S+) \| (\S+)'
        for line in section:
            media_node_find = re.match(regex_media_node, line)
            if media_node_find:
                media_node = media_node_find.group(1)
                confluence_dict[media_node] = {}
            call_leg = re.match(regex_list, line)
            if call_leg:
                call_leg_list = call_leg.group(1, 2, 3, 4, 5, 6)
                confluence_dict[media_node][call_leg_list[0]] = {"ClientType": call_leg_list[1],
                                                                 "IsGateway": call_leg_list[2],
                                                                 "TrackinID": call_leg_list[3],
                                                                 "CorrelationID": call_leg_list[4],
                                                                 "PairedWith": call_leg_list[5]}
        return confluence_dict

    def parse_cascades_section(section):
        """
        Parses the cascades section and outputs: LeftHostname, LeftCascadeDeviceID, LeftCorrelationID,
        RightHostname, RightCascadeDeviceID, RightCorrelationID, LeftTrackingID values in the form of dictionary.
        :param section: contents of the cascades section.
        :return: cascades_dict: a dictionary with the values extracted form the cascades section of the
        summary file.
        """
        cascades_dict = {}
        regex_list = '^\|(.+) \| (0-\d+) \| (.+) \|  <-->  \| (.+) \| (.+) \| (.+) \| (.+)\|$'
        i = 0
        for line in section:
            cascade = re.match(regex_list, line)
            if cascade:
                cascade_list = cascade.group(1, 2, 3, 4, 5, 6, 7)
                cascades_dict[i] = {"LeftHostname": cascade_list[0],
                                    "LeftCascadeDeviceID": cascade_list[1],
                                    "LeftCorrelationID": cascade_list[2],
                                    "RightHostname": cascade_list[3],
                                    "RightCascadeDeviceID": cascade_list[4],
                                    "RightCorrelationID": cascade_list[5],
                                    "LeftTrackingID": cascade_list[6]}
                i = i + 1
        return cascades_dict

    def parse_transcoders_section(section):
        """
        Parses the transcoders section and outputs: TranscoderDeviceID, TranscoderTrackingID, TranscoderID,
        TranscoderURL, LinusHostname, CreationTime, DeletionTime, DeletionReason values in the form of dictionary.
        :param section: contents of the transcoders section.
        :return: transcoders_dict: a dictionary with the values extracted form the transcoders section of the
        """
        transcoders_dict = {}
        if section == "None":
            transcoders_dict = "None"
            return transcoders_dict
        regex_list = '^\| (0-\d+) \| (.+) \| (.+) \| (.+) \| (.+) \| (.+) \| (.+) \| (.+) \|$'
        i = 0
        for line in section:
            transcoders = re.match(regex_list, line)
            if transcoders:
                transcoders_list = transcoders.group(1, 2, 3, 4, 5, 6, 7, 8)
                transcoders_dict[i] = {"TranscoderDeviceID": transcoders_list[0],
                                       "TranscoderTrackingID": transcoders_list[1],
                                       "TranscoderID": transcoders_list[2],
                                       "TranscoderURL": transcoders_list[3],
                                       "LinusHostname": transcoders_list[4],
                                       "CreationTime": transcoders_list[5],
                                       "DeletionTime": transcoders_list[6],
                                       "DeletionReason": transcoders_list[7]}
                i = i + 1
        return transcoders_dict

    summary_file = list(map(str.strip, summary_file))
    locus_id = find_params(summary_file, "LOCUS ID")
    venue_id = find_params(summary_file, "VENUE ID")
    start_time = find_params(summary_file, "START TIME")
    stop_time = find_params(summary_file, "STOP TIME")
    call_type = find_call_type(summary_file)
    confluence_section = parse_sections(summary_file, "**CONFLUENCES**")
    confluence_dict = parse_confluences_section(confluence_section)
    cascade_section = parse_sections(summary_file, "**CASCADES**")
    cascade_dict = parse_cascades_section(cascade_section)
    transcoders_section = parse_sections(summary_file, "**TRANSCODERS**")
    transcoders_dict = parse_transcoders_section(transcoders_section)

    dictionary = {"LocusID": locus_id,
                  "VenueID": venue_id,
                  "StartTime": start_time,
                  "StopTime": stop_time,
                  "CallType": call_type,
                  "Confluences": confluence_dict,
                  "Cascades": cascade_dict,
                  "Transcoders": transcoders_dict}
    return dictionary


path = 'summary'
with open(path, 'r', errors='ignore') as file:
    summary_file = file.readlines()
    summary = parse_summary_file(summary_file)
print(summary)
