# First Project

![Project logo](/logo/logo.png)

Description of First Project - The project is to parse the summary file and use the data to filter out only relevant files in the MC file. 

## Getting Started

Download the summary_reader.py and summary. Run the script with python3 summary_reader.py. The script will parse the summary file and provide the output as a directory.

## Authors

* **Marcin Koszykowski**

